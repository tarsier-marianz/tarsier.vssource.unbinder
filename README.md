# Tarsier VS SourceControl Unbinder
A simple tool for unbinding Visual Studio source control.

![Image1](https://bitbucket.org/tarsier-marianz/tarsier.vssource.unbinder/raw/6074fd21f047d41363297e903014b05e58f62e02/Images/1.png "Tarsier.VSSource.Unbinder")

![Image2](https://bitbucket.org/tarsier-marianz/tarsier.vssource.unbinder/raw/6074fd21f047d41363297e903014b05e58f62e02/Images/2.png "Tarsier.VSSource.Unbinder")

# Credits
![PInvoke](https://bitbucket.org/tarsier-marianz/tarsier.vssource.unbinder/raw/6074fd21f047d41363297e903014b05e58f62e02/Images/credits-pinvoke.jpg?raw=true "PInvoke.Net")
![SQlite](https://bitbucket.org/tarsier-marianz/tarsier.vssource.unbinder/raw/6074fd21f047d41363297e903014b05e58f62e02/Images/credits-sqlite.jpg?raw=true "SQlite")
![FugueIcons](https://bitbucket.org/tarsier-marianz/tarsier.vssource.unbinder/raw/6074fd21f047d41363297e903014b05e58f62e02/Images/credits-fugue.jpg?raw=true "Fugue Icons")